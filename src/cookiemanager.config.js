/* eslint-disable no-unused-vars */

const NECESSARY = 1;
const STATISTICS = 2;
const MARKETING = 3;
const OTHER = 4;

var cookiemanagerconfig = {
    "cookies": [
        {
            "domain": ".doubleclick.net",
            "expires": "Mon, 23 Nov 2020 11:32:08 GMT",
            "expiry": 1606131128,
            "httponly": true,
            "name": "IDE",
            "path": "/",
            "secure": false,
            "value": "AHWqTUmqEyN4DCRhnX8THLCQ2uRG3tslz2ufh13Y8z1ZoxYgu6uw0Fr1-tvWNwQ7",
      "duration": 33696001,
            "category": MARKETING
        },
        {
            "domain": ".adform.net",
            "expires": "Sun, 29 Dec 2019 11:32:08 GMT",
            "expiry": 1577619128,
            "httponly": false,
            "name": "uid",
            "path": "/",
            "secure": false,
            "value": "3836492269164982080",
      "duration": 7776001,
            "category": MARKETING
        },
        {
            "domain": "track.adform.net",
            "expires": "Sun, 29 Dec 2019 11:32:08 GMT",
            "expiry": 1577619128,
            "httponly": false,
            "name": "cid",
            "path": "/",
            "secure": false,
            "value": "3836492269164982080,0,0,0,0",
      "duration": 5184001,
            "category": MARKETING
        },
        {
            "domain": "track.adform.net",
            "expires": "Sat, 30 Nov 2019 11:32:08 GMT",
            "expiry": 1575113528,
            "httponly": false,
            "name": "C",
            "path": "/",
            "secure": false,
            "value": "1",
      "duration": 2592001,
            "category": MARKETING
        },
        {
            "domain": ".facebook.com",
            "expires": "Tue, 28 Jan 2020 11:32:08 GMT",
            "expiry": 1580211128,
            "httponly": true,
            "name": "fr",
            "path": "/",
            "secure": true,
            "value": "0VHTsKJ1SSXRXyaup..BduXS4...1.0.BduXS4.",
      "duration": 33696001,
            "category": MARKETING
        },
        {
            "domain": ".feuerwear.de",
            "expires": "Wed, 30 Oct 2019 11:33:08 GMT",
            "expiry": 1572435188,
            "httponly": false,
            "name": "_gat_UA-8626071-1",
            "path": "/",
            "secure": false,
            "value": "1",
      "duration": 63072001,
            "category": STATISTICS
        },
        {
            "domain": ".ad4mat.de",
            "expires": "Fri, 29 Nov 2019 11:32:08 GMT",
            "expiry": 1575027128,
            "httponly": false,
            "name": "ret_on",
            "path": "/",
            "secure": false,
            "value": "on",
      "duration": 2592001,
            "category": MARKETING
        },
        {
            "domain": ".ad4mat.de",
            "expires": "Fri, 29 Nov 2019 11:32:08 GMT",
            "expiry": 1575027128,
            "httponly": false,
            "name": "config",
            "path": "/",
            "secure": false,
            "value": "eyJtYXJrZXJUaW1lIjp7IjE2MDE1MjI4IjoxNTcyNjA3OTI4fX0%3D",
      "duration": 2592001,
            "category": MARKETING
        },
        {
            "domain": ".ad4mat.de",
            "expires": "Fri, 29 Nov 2019 11:32:08 GMT",
            "expiry": 1575027128,
            "httponly": false,
            "name": "country",
            "path": "/",
            "secure": false,
            "value": ".ad4mat.de",
      "duration": 2592001,
            "category": MARKETING
        },
        {
            "domain": ".ad4mat.de",
            "expires": "Fri, 29 Nov 2019 11:32:08 GMT",
            "expiry": 1575027128,
            "httponly": false,
            "name": "adspaceId",
            "path": "/",
            "secure": false,
            "value": "16015228",
      "duration": 2592001,
            "category": MARKETING
        },
        {
            "domain": ".ad4mat.de",
            "expires": "Fri, 29 Nov q2019 11:32:08 GMT",
            "expiry": 1575027128,
            "httponly": false,
            "name": "ap16015228",
            "path": "/",
            "secure": false,
            "value": "eyJyZXEiOiJleUpoWkhOd1lXTmxTV1FpT2lJeE5qQXhOVEl5T0NJc0ltMTBJam94TENKamIzVnVkSEo1SWpvaWQzZDNMbUZrTkcxaGRDNWtaU0lzSW5Od2NtUWlPaUptWVd4elpTSXNJbU5oZEVsa0lqb2lhRzl0WlNKOSJ9",
      "duration": 2592001,
            "category": MARKETING
        },
        {
            "domain": ".ad4mat.de",
            "expires": "Fri, 29 Nov 2019 11:32:08 GMT",
            "expiry": 1575027128,
            "httponly": false,
            "name": "catId_16015228",
            "path": "/",
            "secure": false,
            "value": "home",
      "duration": 2592001,
            "category": MARKETING
        },
        {
            "domain": ".ad4mat.de",
            "expires": "Fri, 29 Nov 2019 11:32:08 GMT",
            "expiry": 1575027128,
            "httponly": false,
            "name": "multi_adv",
            "path": "/",
            "secure": false,
            "value": "16015228",
      "duration": 2592001,
            "category": MARKETING
        },
        {
            "domain": ".ad4mat.de",
            "expires": "Fri, 29 Nov 2019 11:32:08 GMT",
            "expiry": 1575027128,
            "httponly": false,
            "name": "catId",
            "path": "/",
            "secure": false,
            "value": "home",
      "duration": 2592001,
            "category": MARKETING
        },
        {
            "domain": ".feuerwear.de",
            "expires": "Tue, 28 Jan 2020 11:32:08 GMT",
            "expiry": 1580211128,
            "httponly": false,
            "name": "_fbp",
            "path": "/",
            "secure": false,
            "value": "fb.1.1572435128081.1620987584",
      "duration": 2592001,
            "category": STATISTICS
        },
        {
            "domain": ".feuerwear.de",
            "expires": "Thu, 31 Oct 2019 11:32:07 GMT",
      "duration": 2592001,
            "expiry": 1572521527,
            "httponly": false,
            "name": "_gid",
            "path": "/",
            "secure": false,
            "value": "GA1.2.1136754695.1572435128",
            "category": STATISTICS
        },
        {
            "domain": ".feuerwear.de",
            "expires": "Fri, 29 Oct 2021 11:32:07 GMT",
            "expiry": 1635507127,
            "httponly": false,
            "name": "_ga",
            "path": "/",
            "secure": false,
            "value": "GA1.2.250369628.1572435128",
      "duration": 2592001,
            "category": STATISTICS
        },
        {
            "domain": ".google.com",
            "expires": "Thu, 30 Apr 2020 11:32:07 GMT",
            "expiry": 1588246327,
            "httponly": true,
            "name": "NID",
            "path": "/",
            "secure": false,
            "value": "190=XJo_kR8QPepIKqOfz3HLQj3j7uHYRc6BvttGK728opxUlvuYTLm43fqevft7tGuKfuuqsCnDNVI-dPLoMI2aslp-DAuNb00t0Z9fAA6GsOkrSc0N5ny8twXuJc-MQZD-aNYlY8A8hstbPP13Bm525hdcWXw8Egsy-5MOmUP4lBg",
          "duration": 15811201,
            "category": MARKETING
        },
        {
            "domain": ".bing.com",
            "expires": "Mon, 23 Nov 2020 11:32:07 GMT",
            "expiry": 1606131127,
            "httponly": false,
            "name": "MUID",
            "path": "/",
            "secure": false,
            "value": "03BCD2AC6CE2637B03CFDCA968E2608D",
      "duration": 33696000,
            "category": STATISTICS
        },
        {
            "domain": ".feuerwear.de",
            "expires": "Tue, 28 Jan 2020 11:32:07 GMT",
            "expiry": 1580211127,
            "httponly": false,
            "name": "_gcl_au",
            "path": "/",
            "secure": false,
            "value": "1.1.820046644.1572435127",
      "duration": 7776000,
            "category": NECESSARY
        },
        {
            "domain": "www.feuerwear.de",
            "expires": "Thu, 31 Oct 2019 11:32:06 GMT",
            "expiry": 1572521526,
            "httponly": true,
            "name": "PHPSESSID",
            "path": "/",
            "secure": true,
            "value": "tl76mfvrkekqo4qm4ojngkh0p6",
      "duration": 86400,
            "category": NECESSARY
        }
    ],
    "resources": {
        "js": [
            {
                "url": "https://www.feuerwear.de/assets/min/?f=/assets/templates/fw/js/vendor/jquery-3.3.1.min.js,/assets/templates/fw/js/vendor/blazy-master/blazy.min.js,/assets/templates/fw/js/vendor/lazysizes.min.js,/assets/templates/fw/js/bootstrap2.min.js,/assets/templates/fw/js/vendor/slick/slick.min.js,/assets/templates/fw/js/vendor/fancybox/jquery.fancybox.pack.js,/assets/templates/fw/js/vendor/cover/jquery.cover.js,/assets/templates/fw/js/vendor/ofi.min.js,/assets/templates/fw/js/fwHome.js&d=1572018785"
            },
            {
                "url": "https://www.googletagmanager.com/gtm.js?id=GTM-KH29WP"
            },
            {
                "url": "https://www.feuerwear.de/assets/components/cookiemessage/cookiemessage.js"
            },
            {
                "url": "https://www.googlecommerce.com/trustedstores/api/js"
            },
            {
                "url": "https://www.google.com/_/scs/shopping-verified-reviews-static/_/js/k=boq-shopping-verified-reviews.VerifiedReviewsGcrBootstrapJs.en_US.fvbbYUistQo.es5.O/d=1/ct=zgms/rs=AC8lLkScZLjf0elR5GEufGIEJ_1a7mCH_A/m=bootstrap"
            },
            {
                "url": "https://www.google-analytics.com/analytics.js"
            },
            {
                "url": "https://connect.facebook.net/en_US/fbevents.js"
            },
            {
                "url": "https://www.googleadservices.com/pagead/conversion_async.js"
            },
            {
                "url": "https://bat.bing.com/bat.js"
            },
            {
                "url": "https://www.dwin1.com/15228.js"
            },
            {
                "url": "https://www.google-analytics.com/gtm/js?id=GTM-W493LC3&t=gtm2&cid=250369628.1572435128"
            },
            {
                "url": "https://apis.google.com/js/api.js"
            },
            {
                "url": "https://connect.facebook.net/signals/config/1423389257967741?v=2.9.5&r=stable"
            },
            {
                "url": "https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1046142292/?random=1572435127813&cv=9&fst=1572435127813&num=1&guid=ON&resp=GooglemKTybQhCsO&u_h=600&u_w=800&u_ah=600&u_aw=800&u_cd=32&u_his=1&u_tz=60&u_java=false&u_nplug=0&u_nmime=0&gtm=2wgaa0&ig=1&data=ecomm_pagetype%3Dhome%3Becomm_prodid%3Dundefined%3Becomm_totalvalue%3D&frm=0&url=https%3A%2F%2Fwww.feuerwear.de%2F&tiba=Feuerwear%20-%20Unikate%20aus%20recyceltem%20Feuerwehrschlauch&async=1&rfmt=3&fmt=4"
            },
            {
                "url": "https://www.ad4mat.de/ads/js/ck_tracker.php?adspaceId=16015228&mt=1&country=www.ad4mat.de&sprd=false&catId=home"
            },
            {
                "url": "https://www.google-analytics.com/plugins/ua/ec.js"
            },
            {
                "url": "https://apis.google.com/_/scs/apps-static/_/js/k=oz.gapi.en_US.2O_3XQTFIPY.O/m=gapi_iframes/rt=j/sv=1/d=1/ed=1/am=wQE/rs=AGLTcCM0JjSA0I0wvcxN0q5y4p-sc5Yxiw/cb=gapi.loaded_0"
            }
        ]
    },
    "links": []
};

// eslint-disable-next-line no-undef
YETT_BLACKLIST = [
    {pattern: /www\.googlecommerce\.com/, category: MARKETING},
    {pattern: /www\.google-analytics\.com/, category: STATISTICS},
    {pattern: /www\.googleadservices\.com/, category: MARKETING},
    {pattern: /connect\.facebook\.net/, category: MARKETING},
    {pattern: /bat\.bing\.com/, category: STATISTICS},
    {pattern: /www\.dwin1\.com/, category: MARKETING},
    {pattern: /googleads\.g\.doubleclick\.net/, category: MARKETING},
    {pattern: /apis\.google\.com/, category: NECESSARY},
    {pattern: /www\.google\.com/, category: NECESSARY},
    {pattern: /www\.ad4mat\.de/, category: MARKETING},
];

