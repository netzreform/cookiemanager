import {unblock} from "./unblock";
import {createCookie, getCookie} from "./cookies"
import {patterns} from "./variables";
import 'es6-html-template'
import lang_de from '../locale/de/cookiemanagerwindow';
import lang_en from '../locale/en/cookiemanagerwindow';

class CookieManager {
    constructor(config) {
        let cm = this;
        this.config = config;
        this.windowPrefix = 'ccmRoot';
        if (getCookie('CookieManager') !== null) {
            this.unblockFromCookie();
            return;
        }

        document.addEventListener("DOMContentLoaded", function () {
            cm.renderWindow();
        });
    }

    renderWindow() {
        let content = '';
        this.window = document.createElement('div');
        this.windowId = this.windowPrefix;
        this.window.id = this.windowId;
        let trans = this.getLanguageStrings();

        let cookieconfig = this.getCookiesConfig();

        // language=HTML
        // content = `<div id="ccmBanner">
        //         <div id="ccmBannerContainer">
        //             <div id="ccmBannerTitle">
        //                 ${trans.title}
        //             </div>
        //             <div id="ccmBannerText">
        //                 ${trans.subheadline}
        //             </div>
        //             <div id="ccmBannerActions">
        //                 <div class="ccmBannerActionsItem">
        //                     <button id="ccmBannerConsentAll" class="ccmButton ccmButtonPrimary">${trans.acceptAllCookies}</button>
        //                 </div>
        //                 <div class="ccmBannerActionsItem">
        //                     <button id="ccmBannerSettings" class="ccmButton ccmButtonLink">${trans.cookie_settings}</button>
        //                 </div>
        //             </div>
        //         </div>
        //     </div>
        //     <div id="ccmModal" class="ccmHidden">
        //         <div id="ccmModalContainer">
        //             <div id="ccmModalContent">
        //                 <div id="ccmModalHeader">
        //                     <div id="ccmModalTitle">
        //                     ${trans.cookie_settings}
        //                     </div>
        //                     <div id="ccmModalClose">
        //                         <button class="ccmButton ccmButtonLink">${trans.close}</button>
        //                     </div>
        //                 </div>
        //                 <div id="ccmModalBody">
        //                     ${cookieconfig[1].length > 0 ? `
        //                         <div class="ccmModalSection">
        //                             <div class="ccmModalSectionHeader">
        //                                 <div class="ccmModalSectionTitle">
        //                                     ${trans.headline_necessary}
        //                                 </div>
        //                                 <div class="ccmModalSectionHeaderActions">
        //                                     <label class="ccmSwitch">
        //                                         <input id="ccmCategoryNecessary" class="ccmSwitchCheckbox" type="checkbox" disabled checked />
        //                                         <span class="ccmSwitchToggle"></span>
        //                                     </label>
        //                                 </div>
        //                             </div>
        //                             <div class="ccmModalSectionText">
        //                                 ${trans.description_necessary}
        //                             </div>
        //                             <div class="ccmModalSectionActions">
        //                                 <button class="ccmModalSectionInfoToggle ccmButton ccmButtonLink">${trans.more_info}</button>
        //                             </div>
        //                             <div class="ccmModalSectionInfo ccmHidden">
        //                                 ${cookieconfig[1].map(cookie => `
        //                                     <table class="ccmTable">
        //                                         <tbody>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_name}</th>
        //                                                 <td>${cookie.name}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_provider}</th>
        //                                                 <td>${cookie.domain}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_purpose}</th>
        //                                                 <td>${cookie.info}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_expiry}</th>
        //                                                 <td>${this.durationToText(cookie.duration)}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_type}</th>
        //                                                 <td>HTTP</td>
        //                                             </tr>
        //                                         </tbody>
        //                                     </table>
        //                                 `).join('')}
        //                             </div>
        //                         </div>
        //                     ` : ''}
        //                     ${cookieconfig[2].length > 0 ? `
        //                         <div class="ccmModalSection">
        //                             <div class="ccmModalSectionHeader">
        //                                 <div class="ccmModalSectionTitle">
        //                                     ${trans.headline_preferences}
        //                                 </div>
        //                                 <div class="ccmModalSectionHeaderActions">
        //                                     <label class="ccmSwitch">
        //                                         <input id="ccmCategoryPreferences" class="ccmSwitchCheckbox" type="checkbox" />
        //                                         <span class="ccmSwitchToggle"></span>
        //                                     </label>
        //                                 </div>
        //                             </div>
        //                             <div class="ccmModalSectionText">
        //                                 ${trans.description_preferences}
        //                             </div>
        //                             <div class="ccmModalSectionActions">
        //                                 <button class="ccmModalSectionInfoToggle ccmButton ccmButtonLink">${trans.more_info}</button>
        //                             </div>
        //                             <div class="ccmModalSectionInfo ccmHidden">
        //                                 ${cookieconfig[2].map(cookie => `
        //                                     <table class="ccmTable">
        //                                         <tbody>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_name}</th>
        //                                                 <td>${cookie.name}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_provider}</th>
        //                                                 <td>${cookie.domain}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_purpose}</th>
        //                                                 <td>${cookie.info}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_expiry}</th>
        //                                                 <td>${this.durationToText(cookie.duration)}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_type}</th>
        //                                                 <td>HTTP</td>
        //                                             </tr>
        //                                         </tbody>
        //                                     </table>
        //                                 `).join('')}
        //                             </div>
        //                         </div>
        //                     ` : ''}
        //                     ${cookieconfig[3].length > 0 ? `
        //                         <div class="ccmModalSection">
        //                             <div class="ccmModalSectionHeader">
        //                                 <div class="ccmModalSectionTitle">
        //                                     ${trans.headline_statistics}
        //                                 </div>
        //                                 <div class="ccmModalSectionHeaderActions">
        //                                     <label class="ccmSwitch">
        //                                         <input id="ccmCategoryStatistics" class="ccmSwitchCheckbox" type="checkbox" />
        //                                         <span class="ccmSwitchToggle"></span>
        //                                     </label>
        //                                 </div>
        //                             </div>
        //                             <div class="ccmModalSectionText">
        //                                 ${trans.description_statistics}
        //                             </div>
        //                             <div class="ccmModalSectionActions">
        //                                 <button class="ccmModalSectionInfoToggle ccmButton ccmButtonLink">${trans.more_info}</button>
        //                             </div>
        //                             <div class="ccmModalSectionInfo ccmHidden">
        //                                 ${cookieconfig[3].map(cookie => `
        //                                     <table class="ccmTable">
        //                                         <tbody>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_name}</th>
        //                                                 <td>${cookie.name}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_provider}</th>
        //                                                 <td>${cookie.domain}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_purpose}</th>
        //                                                 <td>${cookie.info}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_expiry}</th>
        //                                                 <td>${this.durationToText(cookie.duration)}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_type}</th>
        //                                                 <td>HTTP</td>
        //                                             </tr>
        //                                         </tbody>
        //                                     </table>
        //                                 `).join('')}
        //                             </div>
        //                         </div>
        //                     ` : ''}
        //                     ${cookieconfig[4].length > 0 ? `
        //                         <div class="ccmModalSection">
        //                             <div class="ccmModalSectionHeader">
        //                                 <div class="ccmModalSectionTitle">
        //                                     ${trans.headline_marketing}
        //                                 </div>
        //                                 <div class="ccmModalSectionHeaderActions">
        //                                     <label class="ccmSwitch">
        //                                         <input id="ccmCategoryMarketing" class="ccmSwitchCheckbox" type="checkbox" />
        //                                         <span class="ccmSwitchToggle"></span>
        //                                     </label>
        //                                 </div>
        //                             </div>
        //                             <div class="ccmModalSectionText">
        //                                 ${trans.description_marketing}
        //                             </div>
        //                             <div class="ccmModalSectionActions">
        //                                 <button class="ccmModalSectionInfoToggle ccmButton ccmButtonLink">${trans.more_info}</button>
        //                             </div>
        //                             <div class="ccmModalSectionInfo ccmHidden">
        //                                 ${cookieconfig[4].map(cookie => `
        //                                     <table class="ccmTable">
        //                                         <tbody>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_name}</th>
        //                                                 <td>${cookie.name}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_provider}</th>
        //                                                 <td>${cookie.domain}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_purpose}</th>
        //                                                 <td>${cookie.info}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_expiry}</th>
        //                                                 <td>${this.durationToText(cookie.duration)}</td>
        //                                             </tr>
        //                                             <tr>
        //                                                 <th>${trans.table_headline_type}</th>
        //                                                 <td>HTTP</td>
        //                                             </tr>
        //                                         </tbody>
        //                                     </table>
        //                                 `).join('')}
        //                             </div>
        //                         </div>
        //                     ` : ''}
        //                 </div>
        //                 <div id="ccmModalFooter">
        //                     <button id="ccmModalConsentSelection" class="ccmButton ccmButtonPrimary">Einstellungen Speichern</button>
        //                 </div>
        //             </div>
        //         </div>
        //     </div>`;

        content = `<div id="ccmBannerModal" class="">
                <div id="ccmBannerModalContainer">
                    <div id="ccmBannerModalContent">
                        <div id="ccmBannerModalHeader">
                            <div id="ccmBannerModalTitle">
                                ${trans.title}
                            </div>
                        </div>
                        <div id="ccmBannerModalBody">
                            <div id="ccmBannerModalText">
                                ${trans.subheadline}
                            </div>    
                        </div>
                        <div id="ccmBannerModalFooter">
                            <div id="ccmBannerModalActions">
                                <div class="ccmBannerModalActionsItem">
                                    <button id="ccmBannerConsentAll" class="ccmButton ccmButtonPrimary">${trans.acceptAllCookies}</button>
                                </div>
                                <div class="ccmBannerModalActionsItem">
                                    <button id="ccmBannerSettings" class="ccmButton ccmButtonLink">${trans.cookie_settings}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ccmModal" class="ccmHidden">
                <div id="ccmModalContainer">
                    <div id="ccmModalContent">
                        <div id="ccmModalHeader">
                            <div id="ccmModalTitle">
                            ${trans.cookie_settings}
                            </div>
                            <div id="ccmModalClose">
                                <button class="ccmButton ccmButtonLink">${trans.close}</button>
                            </div>
                        </div>
                        <div id="ccmModalBody">
                            ${cookieconfig[1].length > 0 ? `
                                <div class="ccmModalSection">
                                    <div class="ccmModalSectionHeader">
                                        <div class="ccmModalSectionTitle">
                                            ${trans.headline_necessary}
                                        </div>
                                        <div class="ccmModalSectionHeaderActions">
                                            <label class="ccmSwitch">
                                                <input id="ccmCategoryNecessary" class="ccmSwitchCheckbox" type="checkbox" disabled checked />
                                                <span class="ccmSwitchToggle"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="ccmModalSectionText">
                                        ${trans.description_necessary}
                                    </div>
                                    <div class="ccmModalSectionActions">
                                        <button class="ccmModalSectionInfoToggle ccmButton ccmButtonLink">${trans.more_info}</button>
                                    </div>
                                    <div class="ccmModalSectionInfo ccmHidden">
                                        ${cookieconfig[1].map(cookie => `
                                            <table class="ccmTable">
                                                <tbody>
                                                    <tr>
                                                        <th>${trans.table_headline_name}</th>
                                                        <td>${cookie.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_provider}</th>
                                                        <td>${cookie.domain}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_purpose}</th>
                                                        <td>${cookie.info}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_expiry}</th>
                                                        <td>${this.durationToText(cookie.duration)}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_type}</th>
                                                        <td>HTTP</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        `).join('')}
                                    </div>
                                </div>
                            ` : ''}
                            ${cookieconfig[2].length > 0 ? `
                                <div class="ccmModalSection">
                                    <div class="ccmModalSectionHeader">
                                        <div class="ccmModalSectionTitle">
                                            ${trans.headline_preferences}
                                        </div>
                                        <div class="ccmModalSectionHeaderActions">
                                            <label class="ccmSwitch">
                                                <input id="ccmCategoryPreferences" class="ccmSwitchCheckbox" type="checkbox" />
                                                <span class="ccmSwitchToggle"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="ccmModalSectionText">
                                        ${trans.description_preferences}
                                    </div>
                                    <div class="ccmModalSectionActions">
                                        <button class="ccmModalSectionInfoToggle ccmButton ccmButtonLink">${trans.more_info}</button>
                                    </div>
                                    <div class="ccmModalSectionInfo ccmHidden">
                                        ${cookieconfig[2].map(cookie => `
                                            <table class="ccmTable">
                                                <tbody>
                                                    <tr>
                                                        <th>${trans.table_headline_name}</th>
                                                        <td>${cookie.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_provider}</th>
                                                        <td>${cookie.domain}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_purpose}</th>
                                                        <td>${cookie.info}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_expiry}</th>
                                                        <td>${this.durationToText(cookie.duration)}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_type}</th>
                                                        <td>HTTP</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        `).join('')}
                                    </div>
                                </div>
                            ` : ''}
                            ${cookieconfig[3].length > 0 ? `
                                <div class="ccmModalSection">
                                    <div class="ccmModalSectionHeader">
                                        <div class="ccmModalSectionTitle">
                                            ${trans.headline_statistics}
                                        </div>
                                        <div class="ccmModalSectionHeaderActions">
                                            <label class="ccmSwitch">
                                                <input id="ccmCategoryStatistics" class="ccmSwitchCheckbox" type="checkbox" />
                                                <span class="ccmSwitchToggle"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="ccmModalSectionText">
                                        ${trans.description_statistics}
                                    </div>
                                    <div class="ccmModalSectionActions">
                                        <button class="ccmModalSectionInfoToggle ccmButton ccmButtonLink">${trans.more_info}</button>
                                    </div>
                                    <div class="ccmModalSectionInfo ccmHidden">
                                        ${cookieconfig[3].map(cookie => `
                                            <table class="ccmTable">
                                                <tbody>
                                                    <tr>
                                                        <th>${trans.table_headline_name}</th>
                                                        <td>${cookie.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_provider}</th>
                                                        <td>${cookie.domain}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_purpose}</th>
                                                        <td>${cookie.info}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_expiry}</th>
                                                        <td>${this.durationToText(cookie.duration)}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_type}</th>
                                                        <td>HTTP</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        `).join('')}
                                    </div>
                                </div>
                            ` : ''}
                            ${cookieconfig[4].length > 0 ? `
                                <div class="ccmModalSection">
                                    <div class="ccmModalSectionHeader">
                                        <div class="ccmModalSectionTitle">
                                            ${trans.headline_marketing}
                                        </div>
                                        <div class="ccmModalSectionHeaderActions">
                                            <label class="ccmSwitch">
                                                <input id="ccmCategoryMarketing" class="ccmSwitchCheckbox" type="checkbox" />
                                                <span class="ccmSwitchToggle"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="ccmModalSectionText">
                                        ${trans.description_marketing}
                                    </div>
                                    <div class="ccmModalSectionActions">
                                        <button class="ccmModalSectionInfoToggle ccmButton ccmButtonLink">${trans.more_info}</button>
                                    </div>
                                    <div class="ccmModalSectionInfo ccmHidden">
                                        ${cookieconfig[4].map(cookie => `
                                            <table class="ccmTable">
                                                <tbody>
                                                    <tr>
                                                        <th>${trans.table_headline_name}</th>
                                                        <td>${cookie.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_provider}</th>
                                                        <td>${cookie.domain}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_purpose}</th>
                                                        <td>${cookie.info}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_expiry}</th>
                                                        <td>${this.durationToText(cookie.duration)}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>${trans.table_headline_type}</th>
                                                        <td>HTTP</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        `).join('')}
                                    </div>
                                </div>
                            ` : ''}
                        </div>
                        <div id="ccmModalFooter">
                            <button id="ccmModalConsentSelection" class="ccmButton ccmButtonPrimary">Einstellungen Speichern</button>
                        </div>
                    </div>
                </div>
            </div>`;

        this.window.innerHTML = content;
        document.body.appendChild(this.window);
        this.registerClickFunctions();
        this.resizeWindow();
    }

    resizeWindow() {
        if (!this.window) return;
        document.getElementsByTagName("body")[0].style.marginBottom = document.getElementById('ccmBanner').clientHeight + 'px';
    }

    getCookiesConfig() {
        let cookieconfig = { 1: [], 2: [], 3: [], 4: [], 5: [] };
        if (undefined === this.config) return cookieconfig;

        for (let i = 0, config; i < this.config.cookies.length; i++) {
            config = this.config.cookies[i];

            if(undefined === config.info) {
                if(this.getBrowserLanguage().match(/de/)) {
                    config.info = config.info_de;
                } else {
                    config.info = config.info_en;
                }
            }

            cookieconfig[config.category].push(config);
        }
        return cookieconfig;
    }

    registerClickFunctions() {
        this.registerConsentAllClick();
        this.registerModalOpenClick();
        this.registerModalCloseClick();
        this.registerInfoToggleClick();
        this.registerConsentSelectionClick();
    }

    registerConsentAllClick() {
        document.getElementById('ccmBannerConsentAll').addEventListener('click', () => {
            let categories = [1,2,3,4];
            createCookie('CookieManager', JSON.stringify({ 'categories': categories }), 365);
            this.unblockFromCookie()
            this.window.classList.add('ccmHidden');
            this.resizeWindow();
        })
    }

    registerModalOpenClick() {
        document.getElementById('ccmBannerSettings').addEventListener('click', () => {
            document.getElementById('ccmModal').classList.remove('ccmHidden');
            document.getElementsByTagName("body")[0].classList.add('ccmModalOpen');
        });
    }

    registerModalCloseClick() {
        document.getElementById('ccmModalClose').addEventListener('click', () => {
            let ccmModal = document.getElementById('ccmModal')
            ccmModal.classList.add('ccmHidden');
            ccmModal.querySelectorAll('.ccmModalSectionInfo').forEach(ccmModalSectionInfo => ccmModalSectionInfo.classList.add('ccmHidden'));
            document.getElementsByTagName("body")[0].classList.remove('ccmModalOpen');
        });
    }

    registerInfoToggleClick() {
        document.getElementById('ccmModal').querySelectorAll('.ccmModalSectionInfoToggle').forEach(ccmModalSectionInfoToggle => {
            ccmModalSectionInfoToggle.addEventListener('click', (e) => {
               ccmModalSectionInfoToggle.parentElement.nextElementSibling.classList.toggle('ccmHidden');
            })
        });    
    }

    registerConsentSelectionClick() {
        document.getElementById('ccmModalConsentSelection').addEventListener('click', () => {
            let categories = [1];
            let checkboxList = {
                'ccmCategoryPreferences' : 2,
                'ccmCategoryStatistics': 3 ,
                'ccmCategoryMarketing' : 4
            };
            for(let elementId in checkboxList) {
                if(document.getElementById(elementId) && document.getElementById(elementId).checked) {
                    categories.push(checkboxList[elementId]);
                }
            }

            createCookie('CookieManager', JSON.stringify({ 'categories': categories }), 365);
            this.unblockFromCookie()

            this.window.classList.add('ccmHidden');
            document.getElementsByTagName("body")[0].classList.remove('ccmModalOpen');
            this.resizeWindow();
        });
    }

    durationToText(duration) {
        let trans = this.getLanguageStrings();

        if (duration == 0) return 'Session';
        let durationtext = '';
        if(duration > 10000)
            duration = Math.ceil(duration / 10000) * 10000;
        if(duration > 1000)
            duration = Math.ceil(duration / 1000) * 1000;
        if(duration > 100)
            duration = Math.ceil(duration / 100) * 100;

        if (duration >= 31540000) {
            durationtext = `${Math.floor(duration / 31540000)} ${Math.floor(duration / 31540000) > 1 ? trans.years : trans.year}`;
        } else if (duration >= 2628000) {
            durationtext = `${Math.floor(duration / 2628000)} ${Math.floor(duration / 2628000) > 1 ? trans.months : trans.month}`;
        } else if (duration >= 604800) {
            durationtext = `${Math.floor(duration / 604800)} ${Math.floor(duration / 604800) > 1 ? trans.weeks : trans.week}`;
        } else if (duration >= 86400) {
            durationtext = `${Math.floor(duration / 86400)} ${Math.floor(duration / 86400) > 1 ? trans.days : trans.day}`;

        } else if (duration >= 3600) {
            durationtext = `${Math.floor(duration / 3600)} ${Math.floor(duration / 3600) > 1 ? trans.hours : trans.hour}`;
        } else {
            durationtext = `${Math.floor(duration / 60)} ${Math.floor(duration / 60) > 1 ? trans.minutes : trans.minute}`;
        }
        return durationtext;
    }

    unblockFromCookie() {
        let CookieCategories = JSON.parse( getCookie('CookieManager') )
        for (let j in CookieCategories.categories) {
            let category = CookieCategories.categories[j]
            for(let i in window.YETT_BLACKLIST) {
                let blacklistitem = window.YETT_BLACKLIST[i]
                if(blacklistitem.category == category) {
                    if(patterns.whitelist === undefined) patterns.whitelist = []
                    patterns.whitelist.push( blacklistitem.pattern )
                    unblock( blacklistitem.pattern )
                }
            }
        }
    }
    getBrowserLanguage() {
        var first = window.navigator.languages
            ? window.navigator.languages[0]
            : null

        var lang = first
            || window.navigator.language
            || window.navigator.browserLanguage
            || window.navigator.userLanguage

        return lang
    }

    getLanguageStrings() {
        if(this.getBrowserLanguage().match(/de/)) {
            return {
                "title":                         "Diese Webseite verwendet Cookies",
                "subheadline":                   "Wählen Sie die Kategorien von Cookies aus, die wir bei Ihrem Besuch verwenden dürfen. Klicken Sie die verschiedenen Kategorien an, um genauere Informationen zu den jeweiligen Cookies zu erhalten.",
                "acceptAllCookies":              "Alle Cookies akzeptieren",
                "showDetails":                   "Details zeigen",
                "hideDetails":                   "Details verbergen",
                "label_necessary":               "Notwendig",
                "headline_necessary":            "Notwendig",
                "description_necessary":   "Notwendige Cookies dienen der Bereitstellung von grundlegenden Funktionen der Website sowie sicherheitsrelevanten Funktionen. Auch Ihre Cookie-Präferenzen werden über ein notwendiges Cookie gespeichert.",
                "labeltitle_necessary":          "Pflichtfeld - kann nicht abgewählt werden. Notwendige Cookies helfen dabei, eine Webseite nutzbar zu machen, indem sie Grundfunktionen wie Seitennavigation und Zugriff auf sichere Bereiche der Webseite ermöglichen. Die Webseite kann ohne diese Cookies nicht richtig funktionieren.",
                "label_preferences":             "Statistiken",
                "headline_preferences":          "Statistiken",
                "description_preferences": "Für die Analyse der Zugriffe auf die Website sowie des Nutzerverhaltens während des Website-Besuchs werden Cookies verwendet. ",
                "labeltitle_preferences":        "Präferenz-Cookies ermöglichen einer Webseite sich an Informationen zu erinnern, die die Art beeinflussen, wie sich eine Webseite verhält oder aussieht, wie z. B. Ihre bevorzugte Sprache oder die Region in der Sie sich befinden.",
                "label_statistics":              "Marketing",
                "headline_statistics":           "Marketing",
                "description_statistics":  "Mithilfe von Marketing Cookie können wir z.B. personalisierte Werbung anbieten oder den Erfolg von Werbeanzeigen messen.",
                "labeltitle_statistics":         "Statistik-Cookies helfen Webseiten-Besitzern zu verstehen, wie Besucher mit Webseiten interagieren, indem Informationen anonym gesammelt und gemeldet werden.",
                "label_marketing":               "Sonstiges",
                "headline_marketing":            "Sonstiges",
                "description_marketing":   "Weitere nicht notwendige Cookies, werden hier aufgelistet. Genauere Informationen zum jeweiligen Zweck finden Sie in der Auflistung.",
                "labeltitle_marketing":          "Marketing-Cookies werden verwendet, um Besuchern auf Webseiten zu folgen. Die Absicht ist, Anzeigen zu zeigen, die relevant und ansprechend für den einzelnen Benutzer sind und daher wertvoller für Publisher und werbetreibende Drittparteien sind.",
                "OK":                            "OK",
                "table_headline_name":           "Name",
                "table_headline_provider":       "Anbieter",
                "table_headline_purpose":        "Zweck",
                "table_headline_expiry":         "Ablauf",
                "table_headline_type":           "Typ",
                "settings": "Einstellungen",
                "cookie_settings": "Cookie Einstellungen",
                "close": "Schließen",
                "more_info": "Mehr Informationen",

                "year":    "Jahr",
                "years":   "Jahre",
                "month":   "Monat",
                "months":  "Monate",
                "week":    "Woche",
                "weeks":   "Wochen",
                "day":     "Tag",
                "days":    "Tage",
                "hour":    "Stunde",
                "hours":   "Stunden",
                "minute":  "Minute",
                "minutes": "Minuten",
            };
        }
        return {
            "title":                         "This website uses cookies.",
            "subheadline":                   "Select the categories of cookies that we may use during your visit. Click on the different categories to find out more about each cookie",
            "acceptAllCookies":              "Accept all cookies",
            "showDetails":                   "Show details",
            "hideDetails":                   "Hide details",
            "label_necessary":               "Necessary",
            "headline_necessary":            "Necessary",
            "trans.description_necessary":   "Necessary cookies serve to provide basic functions of the website as well as security-relevant functions. Your cookie preferences are also stored via a required cookie.",
            "labeltitle_necessary":          "Mandatory field - cannot be deselected. Necessary cookies help to make a website usable by enabling basic functions such as page navigation and access to secure areas of the website. The website cannot function properly without these cookies.",
            "label_preferences":             "Statistics",
            "headline_preferences":          "Statistics",
            "trans.description_preferences": "Cookies are used to analyse access to the website and user behaviour during visits to the website. ",
            "labeltitle_preferences":        "Preference cookies allow a Web site to remember information that affects the way a Web site behaves or looks, such as your preferred language or the region in which you are located.",
            "label_statistics":              "Marketing",
            "headline_statistics":           "Marketing",
            "trans.description_statistics":  "For example, Marketing Cookie may be used to offer personalized advertising or to measure the success of advertisements.",
            "labeltitle_statistics":         "Statistical cookies help Web site owners understand how visitors interact with Web pages by anonymously collecting and reporting information.",
            "label_marketing":               "Other",
            "headline_marketing":            "Other",
            "trans.description_marketing":   "Other unnecessary cookies are listed here. You will find more detailed information on the respective purpose in the list.",
            "labeltitle_marketing":          "Marketing cookies are used to follow visitors to websites. The intent is to display ads that are relevant and appealing to the individual user and are therefore more valuable to publishers and third party advertisers.",
            "OK":                            "OK",
            "table_headline_name":           "Name",
            "table_headline_provider":       "Provider",
            "table_headline_purpose":        "Purpose",
            "table_headline_expiry":         "Procedure",
            "table_headline_type":           "Type",
            "settings": "Settings",
            "cookie_settings": "Cookie Settings",
            "close": "Close",
            "more_info": "More Information",

            "year":    "year",
            "years":   "years",
            "month":   "month",
            "months":  "months",
            "week":    "week",
            "weeks":   "weeks",
            "day":     "day",
            "days":    "days",
            "hour":    "hour",
            "hours":   "hours",
            "minute":  "minute",
            "minutes": "minutes",
        };
    }
}

export const cookiemanagerwindow = new CookieManager(document.cookiemanagerconfig);

