const NECESSARY = 1;
const STATISTICS = 2;
const MARKETING = 3;
const OTHER = 4;

document.cookiemanagerconfig = {
    "cookies": [
        {
            "domain": ".adform.net",
            "name": "uid",
            "duration": 5184002,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": "track.adform.net",
            "name": "cid",
            "duration": 5184002,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": "track.adform.net",
            "name": "C",
            "duration": 2592002,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".doubleclick.net",
            "name": "IDE",
            "duration": 33696002,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".ad4mat.de",
            "name": "ret_on",
            "duration": 2592001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".ad4mat.de",
            "name": "config",
            "duration": 2592001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".ad4mat.de",
            "name": "country",
            "duration": 2592001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".ad4mat.de",
            "name": "adspaceId",
            "duration": 2592001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".ad4mat.de",
            "name": "ap16015228",
            "duration": 2592001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".ad4mat.de",
            "name": "catId_16015228",
            "duration": 2592001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".ad4mat.de",
            "name": "multi_adv",
            "duration": 2592001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".ad4mat.de",
            "name": "catId",
            "duration": 2592001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".feuerwear.de",
            "name": "_gat_UA-8626071-1",
            "duration": 61,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".facebook.com",
            "name": "fr",
            "duration": 7776001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".google.com",
            "name": "NID",
            "duration": 15811201,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".feuerwear.de",
            "name": "_gid",
            "duration": 86400,
            "category": OTHER,
            "info_de": "Registriert eine eindeutige ID, die verwendet wird, um statistische Daten dazu, wie der Besucher die Website nutzt, zu generieren",
            "info_en": "Registers a unique ID, used to generate statistic data on how the visitor uses the website"
        },
        {
            "domain": ".feuerwear.de",
            "name": "_ga",
            "duration": 63072000,
            "category": OTHER,
            "info_de": "Registriert eine eindeutige ID, die verwendet wird, um statistische Daten dazu, wie der Besucher die Website nutzt, zu generieren",
            "info_en": "Registers a unique ID, used to generate statistic data on how the visitor uses the website"
        },
        {
            "domain": ".bing.com",
            "name": "MUID",
            "duration": 33696001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".feuerwear.de",
            "name": "_fbp",
            "duration": 7776001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".feuerwear.de",
            "name": "_gcl_au",
            "duration": 7776001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": "www.feuerwear.de",
            "name": "PHPSESSID",
            "duration": 604799,
            "category": OTHER,
            "info_de": "Session Cookie f\u00fcr sicherheitsrelevante Funktionen",
            "info_en": "Session cookie for security functions"
        },
        {
            "domain": ".atdmt.com",
            "name": "ATN",
            "duration": 63072001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": ".atdmt.com",
            "name": "AA003",
            "duration": 7776001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        },
        {
            "domain": "www.google.com",
            "name": "OTZ",
            "duration": 2592001,
            "category": OTHER,
            "info_de": "",
            "info_en": ""
        }
    ]
};

YETT_BLACKLIST = [
    {js_source: /googleads\.g\.doubleclick\.net/, category: OTHER},
    {js_source: /www\.google-analytics\.com/, category: OTHER}
];
