import {uglify} from 'rollup-plugin-uglify'
import {minify} from 'uglify-es'
import css from 'rollup-plugin-css-porter';
import babel from 'rollup-plugin-babel';

export default {
    input:    'src/index.js',
    output:   [
        {
            file:      'dist/cookiemanager.js',
            format:    'iife',
            name:      'cookiemanager',
            sourcemap: true
        },
        {
            file:    'bundle.min.js',
            format:  'iife',
            name:    'cookiemanager',
            plugins: [uglify({}, minify)]
        }
    ],
    external: ['es6-html-template'],
    plugins:  [
        babel({
            exclude: 'node_modules/**'
        }),
        css(),

    ]
}
